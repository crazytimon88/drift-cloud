import { connect } from 'react-redux'
import { switchResultHidden, SwitchResultHiddenAction } from '../../actions/actions';
import { getTreeNode, isResultHiddenForLevel, pilots } from '../../reducers/reducers';
import TournamentStage from '../../components/app/tournament_stage/tournament_stage';
import { TREE_LEVELS } from '../../models/constants';
import { Dispatch } from 'redux';
import { Pilot } from '../../models/pilot';
import { BracketsNode } from '../../models/brackets_node';

interface StateProps {
  pilots: Pilot[],
  stages: ReadonlyArray<BracketsNode>,
  title: string,
  treeLvl: TREE_LEVELS,
  resultHidden: boolean
}

interface DispatchProps {
  switchResultHidden: (lvl: TREE_LEVELS) => (dispatch: Dispatch<SwitchResultHiddenAction>, getState: any) => any;
}

interface OwnProps {
  treeLvl: TREE_LEVELS,
  title: string,
}

const mapStateToProps = (state: any, ownProps: OwnProps) => ({
  pilots: pilots(state),
  stages: getTreeNode(ownProps.treeLvl)(state),
  title: ownProps.title,
  treeLvl: ownProps.treeLvl,
  resultHidden: isResultHiddenForLevel(ownProps.treeLvl)(state),
});

const mapDispatch = ({
  switchResultHidden
});

export const TournamentStageWrapper = connect<StateProps, DispatchProps, OwnProps>(
  mapStateToProps,
  mapDispatch
)(TournamentStage)