import React from 'react'
import { connect } from 'react-redux'
import { loadMain, switchResultHidden } from '../../actions/actions';
import { isLoaded } from '../../reducers/reducers';
import Tournament from '../../components/app/Tournament'

const App = ({isLoaded}) =>
  isLoaded
    ?
    <Tournament/> :
    <>Loading...</>;

const mapStateToProps = state => ({
  isLoaded: isLoaded(state)
});

export default connect(
  mapStateToProps,
  {
    loadMain,
    switchResultHidden,
  }
)(App)
