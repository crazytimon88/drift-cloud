import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { custommiddleware } from '../middleware/middleware';
import rootReducer from '../reducers/reducers'

const configureStore = preloadedState => {
  const middleware = [ thunk, custommiddleware ];
  const store = createStore(
    rootReducer,
    // preloadedState,
    applyMiddleware(...middleware)
  )

  return store
}

export default configureStore
