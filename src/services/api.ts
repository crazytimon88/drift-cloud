import PilotsDto from "../dto/pilots_dto";
import { TournamentDto } from "../dto/tournament_dto";
import { URLS } from "../models/constants";
import { Pilot, PilotImpl } from "../models/pilot";
import { Tournament, TournamentImpl } from "../models/tournament";

export const API = {
  getPilots: async (): Promise<Pilot[]> => {
    const res = await fetch(URLS.pilots);
    const result = await res.json();
    const dto = result as PilotsDto;
    // return result.pilots.map((pilotRaw) => Pilot.fromJson(pilotRaw));
    return dto.pilots.map((pilotRaw) => PilotImpl.fromJson(pilotRaw));
  },
  getTournament: async (tournamentId?: number): Promise<Tournament> => {
    const res = await fetch(URLS.tournament);
    const tournament = await res.json();
    return TournamentImpl.fromJson(tournament as TournamentDto);
  }
}