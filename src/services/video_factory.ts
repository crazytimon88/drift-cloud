import VideoDto from "../dto/video_dto";
import YoutubeVideoDto from "../dto/youtube_video_dto";
import { UnknownVideoImpl } from "../models/unknown_video";
import { Video } from "../models/video";
import { VideoType } from "../models/video_type";
import { YoutubeVideoImpl } from "../models/youtube_video";

export const parseVideo = (dto: VideoDto): Video => {
  switch (dto.type) {
    case VideoType.Youtube:
      return YoutubeVideoImpl.fromJson(dto.meta as unknown as YoutubeVideoDto);
    case VideoType.Unknown:
    default:
      console.error(`unknown video type = ${dto.type} with meta = ${dto.meta}`)
      return new UnknownVideoImpl();
  }
}