import { VideoType } from "../models/video_type";

export default interface VideoDto {
  type: VideoType;
  meta: Map<any, any>;
}
