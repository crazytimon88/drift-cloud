import VideoDto from "./video_dto";

export default interface RaceDto {
  id: number;
  leaderId: number;
  nextRaceId: null | number; //todo: is it deprecated?
  prevRaceId: null | number; //todo: is it deprecated?
  videoUrls: Array<VideoDto>;
}