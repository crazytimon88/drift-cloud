import RaceDto from "./race_dto";

export interface BracketsNodeDto {
  id: number;
  firstRacerId: number;
  secondRacerId: number;
  isResultHidden: boolean;
  winnerRacerId: number;
  races: ReadonlyArray<RaceDto>;
}
