export default interface PilotDto {
  id: number;
  name: string;
  surname: string;
  avatar: string;
}