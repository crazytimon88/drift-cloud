export default interface YoutubeVideoDto {
  id: string;
  start: number;
  end: number;
  autoplay: number;//todo: special type, could be only 1 or 0
}
