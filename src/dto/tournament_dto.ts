import { BracketsNodeDto } from "./brackets_node_dto";

interface Brackets {
  top32: ReadonlyArray<BracketsNodeDto>;
  top16: ReadonlyArray<BracketsNodeDto>;
  top8: ReadonlyArray<BracketsNodeDto>;
  top4: ReadonlyArray<BracketsNodeDto>;
  top2: ReadonlyArray<BracketsNodeDto>;
}


export interface TournamentDto {
  brackets: Brackets;
}
