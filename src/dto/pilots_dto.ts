import PilotDto from "./pilot_dto";

export default interface PilotsDto {
  pilots: ReadonlyArray<PilotDto>;
}