import { LOAD, loaded } from "../actions/actions";
import { TREE_LEVELS } from "../models/constants";
import { API } from "../services/api";

export const custommiddleware = store => next => action => {

  if (action.type === LOAD) {
    const promise1 = API.getPilots();
    const promise2 = API.getTournament();
    const promise3 = new Promise((resolve, reject) => setTimeout(resolve, 1000));
    return Promise.allSettled([promise1, promise2, promise3]).then((results) => {
      const tops = results[1].value;
      const result = {
        pilots: results[0].value,
      };
      result[TREE_LEVELS['lvl4']] = tops.top32;
      result[TREE_LEVELS['lvl3']] = tops.top16;
      result[TREE_LEVELS['lvl2']] = tops.top8;
      result[TREE_LEVELS['lvl1']] = tops.top4;
      result[TREE_LEVELS['lvl0']] = tops.top2;
      next(
        loaded(result)
      )
    });
  } else {
    return next(action);
  }
}
