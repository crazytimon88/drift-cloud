import { ActionType, BasicActions, loaded } from "../actions/actions";
import { LoadedPayload } from "../actions/loaded_payload";
import { Pilot } from "../models/pilot";
import { Tournament } from "../models/tournament";
import { API } from "../services/api";

const isFulfilled = <T>(input: PromiseSettledResult<T>): input is PromiseFulfilledResult<T> => input.status === 'fulfilled';

export const custommiddleware = (store: any) => (next: any) => (action: BasicActions) => {

  if (action.type === ActionType.Load) {
    const promise1 = API.getPilots();
    const promise2 = API.getTournament();
    const promise3 = new Promise((resolve, reject) => setTimeout(resolve, 1000));

    return Promise.allSettled([promise1, promise2, promise3]).then((results) => {
      const fulfilledPromises = results.filter(isFulfilled);
      next(
        loaded(new LoadedPayload(fulfilledPromises[1].value as Tournament, fulfilledPromises[0].value as Pilot[]))
      )
    });
  } else {
    return next(action);
  }
}