import { FormControlLabel, FormGroup, Switch } from "@mui/material";
import React from 'react';
import RaceNodeCmp from "../../race_node/race_node";
import { TREE_LEVELS } from "../../../models/constants";
import { Dispatch } from "redux";
import { Pilot } from "../../../models/pilot";
import { BracketsNode } from "../../../models/brackets_node";
import { SwitchResultHiddenAction } from "../../../actions/actions";

type Props = {
  pilots: Array<Pilot>;
  stages: ReadonlyArray<BracketsNode>,
  title: string;
  resultHidden: boolean;
  switchResultHidden: (lvl: TREE_LEVELS) => (dispatch: Dispatch<SwitchResultHiddenAction>, getState: any) => any;
  treeLvl: TREE_LEVELS;//todo: migrate to TS enum
  //(lvl: React.SyntheticEvent, checked: boolean) => void;
}

const TournamentStage = ({ pilots, stages, title, resultHidden, switchResultHidden, treeLvl }: Props) => {
  return (
    <>
      <FormGroup>
        <FormControlLabel
          control={
            <Switch size="small"
              checked={!resultHidden}
              onChange={() => switchResultHidden(treeLvl)}
            />
          }
          label="Result"
          className="column-caption-switch-text"
          disableTypography={true}
          labelPlacement="start" />
      </FormGroup>
      <div className="column-caption">
        {title}
      </div>
      {stages.map(
        (node, index) => {
          const _firstPilot = pilots.filter((pilot) => pilot.id === node.firstRacerId)[0];
          const _secondPilot = pilots.filter((pilot) => pilot.id === node.secondRacerId)[0];
          const _videoUrls = node.races.map((race) => race.videoUrls.map((x) => x.toUrl())).flat() || ['asd']
          return (
            <div key={index}>
              <RaceNodeCmp
                resultHidden={resultHidden}
                firstPilot={_firstPilot}
                secondPilot={_secondPilot}
                videoUrls={_videoUrls}
              />
            </div>
          )
        }
      )
      }
    </>
  );
}

export default TournamentStage;