import React from 'react';
import './Tournament.css';
import { TournamentStageWrapper } from "../../containers/tournament_stage/tournament_stage";
import { TREE_LEVELS } from "../../models/constants";

const Tournament = () => {
  return (
  <div className="App">
      <div className="container">
        <div className="top32">
          <TournamentStageWrapper
            treeLvl={TREE_LEVELS.lvl4}
            title="top 32"
          />
        </div>
        <div className="top16">
          <TournamentStageWrapper
            treeLvl={TREE_LEVELS.lvl3}
            title="top 16"
          />
        </div>
        <div className="top8">
          <TournamentStageWrapper
            treeLvl={TREE_LEVELS.lvl2}
            title="top 8"
          />
        </div>
        <div className="top4">
          <TournamentStageWrapper
            treeLvl={TREE_LEVELS.lvl1}
            title="top 4"
          />
        </div>
        <div className="top2">
          <TournamentStageWrapper
            treeLvl={TREE_LEVELS.lvl0}
            title="top 2"
          />
        </div>
      </div>
    </div>
  )
}

export default Tournament;