import './race_node.css';
import React, { useEffect, useState } from 'react';
import YouTubeIcon from '@mui/icons-material/YouTube';
import VisibilityIcon from '@mui/icons-material/Visibility';
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff';
import { Box, Button, ButtonGroup, Card, CardActions, Checkbox, Dialog } from '@mui/material';
import { usePrevious } from '../../common/hooks/use_previous';
import * as models from '../../models/pilot';
import Pilot from './components/pilot/pilot';

type Props = {
  resultHidden: boolean,
  firstPilot: models.Pilot,
  secondPilot: models.Pilot,
  videoUrls: Array<string>,
}

const RaceNodeCmp = ({resultHidden, firstPilot, secondPilot, videoUrls}: Props) => {
  const [showVideoTip, setShowVideoTip] = useState(false);
  const [isResultHidden, setIsResultHidden] = useState(true);
  const [currentVideo, setCurrentVideo] = useState<string|undefined>();

  const showVideo = (videoUrl:string) => {
    setCurrentVideo(videoUrl);
    setShowVideoTip(true);
  }
  const hideVideo = () => setShowVideoTip(false);
  const switchResultvisibility = (forcedValue: boolean | undefined | null) =>
    setIsResultHidden(forcedValue != null ? forcedValue : !isResultHidden);

  let prevResultHidden = usePrevious(resultHidden);

  useEffect(() => {
    if (resultHidden !== prevResultHidden) {
      switchResultvisibility(resultHidden);
    }
  });

  return (
    <div className="RaceNodeCmp">
      <Card variant="outlined" sx={{ display: 'flex', width: 350 }}>
        <Box sx={{ display: 'flex', flexDirection: 'row', alignItems: 'center', width: 200 }}>
          {firstPilot && <Pilot
            avatar={firstPilot.avatar}
            name={firstPilot.name}
            surname={firstPilot.surname}
            isDataHidden={isResultHidden}/>}
          VS
          {secondPilot && <Pilot
            avatar = {secondPilot.avatar}
            name = {secondPilot.name}
            surname={secondPilot.surname}
            isDataHidden={isResultHidden}/>}

        <CardActions disableSpacing  sx={{ display: 'flex', flexDirection: 'column', width: 150 }}>
          <ButtonGroup color="error" size="small" variant="text" aria-label="outlined primary button group">
            {videoUrls.map((value, index) => {
              return (
                <Button key={index} onClick={() => showVideo(value)}>
                  <YouTubeIcon fontSize="large" />
                </Button>
              )
            })}
          </ButtonGroup>
          <Checkbox
            checked={!isResultHidden}
            icon={<VisibilityOffIcon />}
            checkedIcon={<VisibilityIcon />}
            onChange={switchResultvisibility.bind(this, null)} />
        </CardActions>
        </Box>

      </Card>
      <Dialog onClose={hideVideo.bind(this)} open={showVideoTip} fullWidth={true} maxWidth="md">
        <iframe
          width="900"
          height="510"
          src={currentVideo}
          title="Drift Cloud"
          frameBorder="0"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen>
        </iframe>
      </Dialog>
    </div>
  );
}

export default RaceNodeCmp;