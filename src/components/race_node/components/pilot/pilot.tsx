import './pilot.css';
import React from 'react';
import Avatar from '@mui/material/Avatar';

type Props = {
  name: string,
  surname: string,
  avatar: string,
  isDataHidden: boolean,
}

const Pilot = ({name, surname, avatar, isDataHidden}: Props) => {
  const avatarSize = 60;
  let fullname = isDataHidden
    ? 'Hidden'
    : `${name[0]}.${surname}`;
  return (
    <div className="pilot-container">
      <Avatar
        style={
          isDataHidden ? {} : { display: 'none' }
        }
        alt="Unknown"
        sx={{ width: avatarSize, height: avatarSize }}
      >?</Avatar>
      <Avatar
        style={
          isDataHidden ? { display: 'none' } : {}
        }
        alt={name + surname}
        src={avatar}
        sx={{ width: avatarSize, height: avatarSize }}
      />
      <div className="pilot-name">
        {fullname}
      </div>
    </div>);
}

export default Pilot;