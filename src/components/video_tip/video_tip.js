import './video_tip.css';
import React from 'react';
import { CloseBtn } from '../../common/close_btn/close_btn';

export class VideoTip extends React.Component {

    render() {
      return (
        <div className="VideoTip">
          <CloseBtn onClick={this.props.onClose}/>
          <iframe
              width="672"
              height="376"
              src={this.props.videoUrl}
              title="YouTube video player"
              frameBorder="0"
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen>
          </iframe>
        </div>
      );
    }
  }
