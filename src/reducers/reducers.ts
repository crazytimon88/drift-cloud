import { combineReducers } from 'redux'
import { TREE_LEVELS } from '../models/constants';
import { BracketsNode } from '../models/brackets_node';
import { ActionType, BasicActions } from '../actions/actions';
import { Pilot } from '../models/pilot';

type TopDeclaration = {
  resultHidden: boolean,
  top: ReadonlyArray<BracketsNode>,
}

type InitialState = {
  isLoaded: boolean,
  pilots?: Pilot[],
  'top32'?: TopDeclaration,
  'top16'?: TopDeclaration,
  'top8'?: TopDeclaration,
  'top4'?: TopDeclaration,
  'top2'?: TopDeclaration,
}

const initialState: InitialState = {
  isLoaded: false,
}

export const isLoaded = (state: any) => state.basic.isLoaded;

export const pilots = (state: any) => {
  return state.basic.pilots;
};

export const getTreeNode = (lvl: TREE_LEVELS) => (state: any): ReadonlyArray<BracketsNode> => {
  return state.basic[lvl].top;
};

export const isResultHiddenForLevel = (lvl: TREE_LEVELS) => (state: any) => {
  return state.basic[lvl].resultHidden;
};

export const top32 = (state: any) => {
  return state.basic.top32;
};

export const top16 = (state: any) => {
  return state.basic.top16;
};

export const top8 = (state: any) => {
  return state.basic.top8;
};

export const top4 = (state: any) => {
  return state.basic.top4;
};

export const top2 = (state: any) => {
  return state.basic.top2;
};

const basic = (state:InitialState = initialState, action: BasicActions):InitialState => {
  if (action.type === ActionType.Loaded) {
    return {
      pilots: action.payload.pilots,
      top32: {
        resultHidden: true,
        top: action.payload.tournament.top32,
      },
      top16: {
        resultHidden: true,
        top: action.payload.tournament.top16,
      },
      top8: {
        resultHidden: true,
        top: action.payload.tournament.top8,
      },
      top4: {
        resultHidden: true,
        top: action.payload.tournament.top4,
      },
      top2: {
        resultHidden: true,
        top: action.payload.tournament.top2,
      },
      isLoaded: true,
    }
  }

  if (action.type === ActionType.SwitchResultHidden) {
    const lvl = _mapLvlToTops(action.payload);
    const existingStateWithAnyType:any = state;
    const updatedState = {
      ...existingStateWithAnyType,
      lvl: {...existingStateWithAnyType[lvl]}
    };
    updatedState[lvl].resultHidden = !existingStateWithAnyType[lvl].resultHidden;
    return updatedState;
  }

  return state
}

const _mapLvlToTops = (lvl: TREE_LEVELS) => {
  const mapping = new Map<TREE_LEVELS, string>();
  mapping.set(TREE_LEVELS.lvl4, 'top32');
  mapping.set(TREE_LEVELS.lvl3, 'top16');
  mapping.set(TREE_LEVELS.lvl2, 'top8');
  mapping.set(TREE_LEVELS.lvl1, 'top4');
  mapping.set(TREE_LEVELS.lvl0, 'top2');
  return mapping.get(lvl) as string;
}

const rootReducer = combineReducers({
  basic,
})

export default rootReducer
