import { Dispatch } from "redux";
import { TREE_LEVELS } from "../models/constants";
import { LoadedPayload } from "./loaded_payload";

export enum ActionType {
  Load = 'load',
  Loaded = 'loaded',
  SwitchResultHidden = 'switchResultHiddenAction',
}

export type SwitchResultHiddenAction = {
  type: typeof ActionType.SwitchResultHidden,
  payload: TREE_LEVELS,
}

export type LoadedAction = {
  type: typeof ActionType.Loaded,
  payload: LoadedPayload,
}

export type LoadAction = {
  type: typeof ActionType.Load,
}

export type BasicActions = LoadAction | LoadedAction | SwitchResultHiddenAction;

export const loaded = (payload: LoadedPayload): LoadedAction => {
  console.log(payload);
  return {
    type: ActionType.Loaded,
    payload
  }
};

export const loadMain = () => (dispatch: Dispatch<LoadAction>, getState: any) => {
  dispatch({
    type: ActionType.Load
  })
};

export const switchResultHidden = (lvl: TREE_LEVELS) => (dispatch: Dispatch<SwitchResultHiddenAction>, getState: any) => {
  dispatch({
    type: ActionType.SwitchResultHidden,
    payload: lvl
  })
};
