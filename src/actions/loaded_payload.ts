import { Pilot } from "../models/pilot";
import { Tournament } from "../models/tournament";
import { ActionPayload } from "./action_payload";

export class LoadedPayload implements ActionPayload {
  pilots: Pilot[];
  tournament: Tournament;

  constructor(tournament: Tournament, pilots: Pilot[]) {
    this.tournament = tournament;
    this.pilots = pilots;
  }
}