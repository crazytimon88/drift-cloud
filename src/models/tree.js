import RaceNode from "./race_node";
import Pilot from "./pilot";

export class Tree {
  _pilots = [];
  constructor(pilots) {
    this._pilots = this._generatePilots();
    pilots.forEach(element => {
      return new RaceNode(element, this._pilots);
    });
    // let parentNode;
    // for (let i = 0; i <= 16; i = i + 2) {
    //   parentNode = new RaceNode(parentNode, pilots[i], pilots[i+1]);
    // }

    let createNode = function (level) {
      let node = new RaceNode();
      if (level === 0 ) {
        return node;
      } else {
        let updatedLevel = level - 1;
        node.childNode1 = createNode(updatedLevel);
        node.childNode2 = createNode(updatedLevel);
        return node;
      }
    }

    createNode(5);

  }

  _nameArr = [
    'Robert', 'Matthew', 'Daniel', 'Christopher', 'Joshua', 'James', 'Michael', 'Jason', 'Joseph', 'David', 'John', 'Andrew', 'Brian', 'William', 'Ryan', 'Eric', 'Anthony', 'Justin', 'Travis', 'Scott', 'Brandon', 'Adam', 'Jeremy', 'Aaron', 'Stephen', 'Benjamin', 'Timothy', 'Jesse', 'Charles', 'Steven', 'Kyle', 'Kenneth'
  ];



  _generatePilots() {
    let result = [];
    for (let i = 0; i <= 31; i++) {
      let pilot = new Pilot(i, this._nameArr[i]);
      result.push(pilot);
    }
    return result;
  }
}

        //                    2
        //         4                   4
        //      8    8               8    8
        //    16    16    16         16   16   16
        // 32    32    32    32   32   32   32   32   32   32   32   32   32   32   32   32


        //  o
        //     o
        //
        //
        //
