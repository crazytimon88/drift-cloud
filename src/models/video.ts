import { VideoType } from "./video_type";

export interface Video {
  type: Readonly<VideoType>;
  toUrl(): string;
}