import { Video } from "./video";
import { VideoType } from "./video_type";

export class UnknownVideoImpl implements Video {
  type = VideoType.Unknown;
  toUrl(): string {
    return 'unknown vide url'
  }
}