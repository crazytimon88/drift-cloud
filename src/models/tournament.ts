import { TournamentDto } from "../dto/tournament_dto";
import { BracketsNode, BracketsNodeImpl } from "./brackets_node";

export interface Tournament {
  top32: ReadonlyArray<BracketsNode>;
  top16: ReadonlyArray<BracketsNode>;
  top8: ReadonlyArray<BracketsNode>;
  top4: ReadonlyArray<BracketsNode>;
  top2: ReadonlyArray<BracketsNode>;
}

export class TournamentImpl implements Tournament {
  top32: ReadonlyArray<BracketsNode> = [];
  top16: ReadonlyArray<BracketsNode> = [];
  top8: ReadonlyArray<BracketsNode> = [];
  top4: ReadonlyArray<BracketsNode> = [];
  top2: ReadonlyArray<BracketsNode> = [];

  constructor(
    top32: ReadonlyArray<BracketsNode>,
    top16: ReadonlyArray<BracketsNode>,
    top8: ReadonlyArray<BracketsNode>,
    top4: ReadonlyArray<BracketsNode>,
    top2: ReadonlyArray<BracketsNode>,
  ) {
    this.top32 = top32;
    this.top16 = top16;
    this.top8 = top8;
    this.top4 = top4;
    this.top2 = top2;
  }

  static fromJson(json: TournamentDto) {
    return new TournamentImpl(
      json.brackets.top32.map(BracketsNodeImpl.fromJson),
      json.brackets.top16.map(BracketsNodeImpl.fromJson),
      json.brackets.top8.map(BracketsNodeImpl.fromJson),
      json.brackets.top4.map(BracketsNodeImpl.fromJson),
      json.brackets.top2.map(BracketsNodeImpl.fromJson)
    );
  };
}