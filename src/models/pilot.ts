import PilotDto from "../dto/pilot_dto";

interface PilotConstructor {
  new (id: number, name: string, surname: string, avatar: string): void;
  fromJson(json: PilotDto): Pilot;
}

export interface Pilot {
  id: number;
  name: string;
  surname: string;
  avatar: string;
}

export const PilotImpl: PilotConstructor = class PilotImpl implements Pilot {
  id;
  name;
  surname;
  avatar;

  constructor(id: number, name: string, surname: string, avatar: string) {
    this.id = id;
    this.name = name;
    this.avatar = avatar;
    this.surname = surname;
  }

  static fromJson(json: PilotDto): Pilot {
    return new PilotImpl(json.id, json.name, json.surname, json.avatar)
  }
}