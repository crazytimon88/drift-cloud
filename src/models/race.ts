import RaceDto from "../dto/race_dto";
import { parseVideo } from "../services/video_factory";
import { Video } from "./video";

interface RaceConstructor {
  new (
    id: number,
    leaderId: number,
    nextRaceId: null | undefined | number, //todo: is it deprecated?
    prevRaceId: null | undefined | number, //todo: is it deprecated?
    videoUrls: Array<Video>
  ): void;

  fromJson(json: RaceDto): Race;
}

export interface Race {
  id: number;
  leaderId: number;
  nextRaceId: null | undefined | number; //todo: is it deprecated?
  prevRaceId: null | undefined | number; //todo: is it deprecated?
  videoUrls: Array<Video>;
}

export const RaceImpl: RaceConstructor = class RaceImpl implements Race {
  id: number;
  leaderId: number;
  nextRaceId: null | undefined | number; //todo: is it deprecated?
  prevRaceId: null | undefined | number; //todo: is it deprecated?
  videoUrls: Array<Video> = [];

  constructor(
    id: number,
    leaderId: number,
    nextRaceId: null | undefined | number, //todo: is it deprecated?
    prevRaceId: null | undefined | number, //todo: is it deprecated?
    videoUrls: Array<Video> = [],
  ) {
    this.id = id;
    this.leaderId = leaderId;
    this.nextRaceId = nextRaceId;
    this.prevRaceId = prevRaceId;
    this.videoUrls = videoUrls;
  }

  static fromJson(json: RaceDto): Race {
    return new RaceImpl(
      json.id,
      json.leaderId,
      json.nextRaceId,
      json.prevRaceId,
      json.videoUrls.map(parseVideo),
    )
  }
}