import YoutubeVideoDto from "../dto/youtube_video_dto";
import { Video } from "./video";
import { VideoType } from "./video_type";

interface YoutubeVideoConstructor {
  new (id: string, start: number, end: number, autoplay: number): void;
  fromJson(json: YoutubeVideoDto): YoutubeVideo;
}

enum Autoplay {
  Yes = 1,
  No = 0,
}

export interface YoutubeVideo extends Video {
  id: string;
  start: number;
  end: number;
  autoplay: Autoplay;//todo: special type, could be only 1 or 0
}

export const YoutubeVideoImpl: YoutubeVideoConstructor = class YoutubeVideoImpl implements YoutubeVideo {
  id;
  start;
  end;
  autoplay = 1;
  type = VideoType.Youtube;

  constructor(
    id: string,
    start: number,
    end: number,
    autoplay: Autoplay,
  ) {
    this.id = id;
    this.start = start;
    this.end = end;
    this.autoplay = autoplay;
  }


  static fromJson(json: YoutubeVideoDto): YoutubeVideo {
    return new YoutubeVideoImpl(
      json.id,
      json.start,
      json.end,
      json.autoplay,
    )
  }

  toUrl(): string {
    return `https://www.youtube.com/embed/${this.id}?start=${this.start}&end=${this.end}&autoplay=${this.autoplay}`;
  }
}