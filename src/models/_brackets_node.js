import Race from "./race";

export default class BracketsNode {
  id = null;
  firstRacerId = null;
  secondRacerId = null;
  isResultHidden = true;
  winnerRacerId = null;
  races = [];

  constructor(
    id,
    firstRacerId,
    secondRacerId,
    isResultHidden,
    winnerRacerId,
    races,
  ) {
    this.id = id;
    this.firstRacerId = firstRacerId;
    this.secondRacerId = secondRacerId;
    this.isResultHidden = isResultHidden;
    this.winnerRacerId = winnerRacerId;
    this.races = races;
  }

  static fromJson(json) {
    return new BracketsNode(
      json['id'],
      json['firstRacerId'],
      json['secondRacerId'],
      json['isResultHidden'],
      json['winnerRacerId'],
      json['races'].map(Race.fromJson),
    )
  }
}
