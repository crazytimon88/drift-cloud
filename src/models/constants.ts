export const URLS = {
  pilots: 'mocks/pilots.json',
  tournament: 'mocks/tournament.json'
}

export const enum TREE_LEVELS {
  lvl4 = 'top32',
  lvl3 = 'top16',
  lvl2 = 'top8',
  lvl1 = 'top4',
  lvl0 = 'top2'
}