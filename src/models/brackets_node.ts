import { BracketsNodeDto } from "../dto/brackets_node_dto";
import { Race, RaceImpl } from "./race";

interface BracketsNodeConstructor {
  new (
    id: number,
    firstRacerId: number,
    secondRacerId: number,
    isResultHidden: boolean,
    winnerRacerId: number,
    races: ReadonlyArray<Race>,
  ): void;

  fromJson(json: BracketsNodeDto): BracketsNode;
}

export interface BracketsNode {
  id: number;
  firstRacerId: number;
  secondRacerId: number;
  isResultHidden: boolean;
  winnerRacerId: number;
  races: ReadonlyArray<Race>;
}

export const BracketsNodeImpl: BracketsNodeConstructor = class BracketsNodeImpl implements BracketsNode {
  id: number;
  firstRacerId: number;
  secondRacerId: number;
  isResultHidden: boolean;
  winnerRacerId: number;
  races: ReadonlyArray<Race>;

  constructor(
    id: number,
    firstRacerId: number,
    secondRacerId: number,
    isResultHidden: boolean,
    winnerRacerId: number,
    races: ReadonlyArray<Race>,
  ) {
    this.id = id;
    this.firstRacerId = firstRacerId;
    this.secondRacerId = secondRacerId;
    this.isResultHidden = isResultHidden;
    this.winnerRacerId = winnerRacerId;
    this.races = races;
  }

  static fromJson(json: BracketsNodeDto): BracketsNode {
    return new BracketsNodeImpl(
      json.id,
      json.firstRacerId,
      json.secondRacerId,
      json.isResultHidden,
      json.winnerRacerId,
      json.races.map(RaceImpl.fromJson),
    )
  }
}