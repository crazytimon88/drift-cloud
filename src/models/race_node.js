export default class RaceNode {
// class RaceNode {
  id = null;
  childNode1 = null;
  childNode2 = null;
  pilot1 = null;
  pilot2 = null;
  videoUrls = [];

  constructor(id, childNode1, childNode2, pilot1, pilot2, videoUrls) {
    this.id = id;
    this.childNode1 = childNode1;
    this.childNode2 = childNode2;
    this.pilot1 = pilot1;
    this.pilot2 = pilot2;
    this.videoUrls = videoUrls;
  }
}
